import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity } from 'react-native';
import React, { useEffect, useState } from 'react';
import Featur from '../components/Example';
import Arrow from '../assets/images/arrowf.svg'

const Game = () => {



  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.headertxt}>Winner</Text>
      </View>
      <Text style={styles.headertxt1}>Customers who won prize in Lucky Draw Contests</Text>
      <ScrollView>
        <View style={styles.product}>
          {
            Featur.map((item,i) => {
              return (
                <TouchableOpacity style={styles.productdel}>
                  <Image source={{ uri: item?.image }} style={styles.productimg} />
                  <Text style={{ fontSize: 18, color: 'orange', fontWeight: 'bold', alignSelf: 'center' }}>Congratulations</Text>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 5 }}>
                    <Image source={{ uri: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg' }}
                      style={{ height: 35, width: 35, borderRadius: 40 }} />
                    <Text style={{ color: 'black', margin: 7 }}>Unknown</Text>
                  </View>
                  <Text style={{ fontSize: 12.5, color: 'black', fontWeight: '500', alignSelf: 'center' }}>Winner of Bajaj Dominar 400</Text>
                </TouchableOpacity>
              );
            })
          }
        </View>
      </ScrollView>
    </View>
  );
};

export default Game;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },
  product: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    marginHorizontal: 10,
    // paddingHorizontal: 5,
    paddingTop: 25,

  },
  productdel: {
    height: 270,
    width: '45%',
    margin: 10,
    borderRadius: 10,
    backgroundColor: 'white'
  },
  productimg: {
    height: '60%',
    width: '99.8%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    resizeMode: 'stretch',
  },
  headertxt: {
    color: 'black',
    fontSize: 23,
    fontWeight: 'bold',
    marginHorizontal: 20,
    marginTop: 10,
  },
  headertxt1: {
    color: 'black',
    fontSize: 22,
    fontWeight: '900',
    marginHorizontal: 20,
    marginTop: 10,
  },
})