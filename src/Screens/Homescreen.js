/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
import { FlatList, StyleSheet, Text, TouchableOpacity, View, Image, TextInput, Dimensions, ScrollView } from 'react-native'
import React from 'react'
import Header from '../components/Header'
import SliderItem from '../components/SliderItem';
import Feature from '../components/Feature';
import Svg, { Circle } from 'react-native-svg';


const Homescreen = () => {
    const width = 110;
    const height = 110;
    const size = width < height ? width - 32 : height - 16;
    const strokeWidth = 25;
    const radius = (size - strokeWidth) / 2;
    const circunference = radius * 2 * Math.PI;
    return (
        <View style={styles.container}>
            <Header />
            <ScrollView style={{ width: '100%', }}>
                <SliderItem />
                <Feature />
                <View style={{ height: '35%', width: '100%', backgroundColor: 'white',marginTop:10}}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '97%',}}>
                        <Text style={{ color: 'black', fontSize: 18, fontWeight: '500', margin: 10 }}>Our Best Product</Text>
                        <TouchableOpacity>
                            <Text style={{ color: 'blue', paddingTop: 15 }}> View all</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-end', backgroundColor: 'white', height: '100%', }}>
                        <Image source={{ uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Blue_Tshirt.jpg/220px-Blue_Tshirt.jpg' }} style={{ height: 210, width: 156, marginTop: '8%', marginRight: '5%' }} />
                        <View style={{ height: 100, width: 100, alignItems: 'center', justifyContent: 'center', marginRight: '5%' }}>
                            <View style={{ position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: 'black', fontSize: 10, fontWeight: '500' }}>Sold</Text>
                                <Text style={{ color: '#FFA500', fontSize: 14, fontWeight: '500' }}>400</Text>
                                <Text style={{ color: 'black', fontSize: 10, fontWeight: '500' }}>Out of</Text>
                                <Text style={{ color: 'black', fontSize: 14, fontWeight: '500' }}>1000</Text>
                            </View>
                            <Svg width={width} height={size} style={{ marginLeft: 10 }} >
                                <Circle
                                    strokeWidth="5"
                                    stroke="#FFA500"
                                    fill="none"
                                    cx='50'
                                    cy='50'
                                    r='40'
                                    strokeDasharray={`${circunference} ${circunference}`}>
                                </Circle>
                            </Svg>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>

        </View>
    );
};

export default Homescreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    banner: {
        width: '98%',
        height: 30,
    },
})