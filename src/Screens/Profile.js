import { StyleSheet, Text, View, Image, TextInput, ScrollView, TouchableOpacity } from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

const Profile = () => {
  return (
    <ScrollView style={{ flex: 1 }}>
      <LinearGradient colors={['#C068A6', '#9E366F', '#CF0D5E']} style={styles.profilegradient}>
        <View style={{ flexDirection: 'row', margin: 45 }}>
          <Image source={{ uri: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg' }}
            style={{ height: 100, width: 100, borderRadius: 60, }} />
          <Text style={{ fontSize: 25, margin: 25, fontWeight: '500', color: 'white' }}>John</Text>
        </View>
      </LinearGradient>
      <Text style={styles.header}>Mobile</Text>
      <View style={styles.input}>
        <Text style={{ color: 'black', marginHorizontal: '5%', fontSize: 16 }}>John</Text>
      </View>

      <Text style={styles.header}>Email</Text>
      <View style={styles.input}>
        <Text style={styles.inputtxt}>John@gmail.com</Text>
      </View>
      <Text style={styles.header}>Date of birth</Text>
      <View style={styles.input}>
        <Text style={styles.inputtxt}>22 Feb 1997</Text>
      </View>
      <Text style={styles.header}>Gender</Text>
      <View style={styles.input}>
        <Text style={styles.inputtxt}>Male</Text>
      </View>
      <Text style={styles.header}>Location</Text>
      <View style={styles.input}>
        <Text style={styles.inputtxt}>Kozhikode</Text>
      </View>
      <TouchableOpacity>
        <LinearGradient colors={['#C068A6', '#9E366F', '#CF0D5E']}
          style={styles.updategradient}>
          <Text style={styles.updatetxt}>Update Profile</Text>
        </LinearGradient>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  header: {
    color: 'black',
    marginHorizontal: '6%',
    fontSize: 14,
    marginTop: '10%',
  },
  input: {
    height: 30,
    width: '90%',
    alignSelf: 'center',
    borderRadius: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    color: 'black',
    fontSize: 17,
    marginTop: 5
  },
  updategradient: {
    paddingLeft: 15, height: 50, width: '80%',
    paddingRight: 15,
    borderRadius: 10,
    alignSelf: 'center',
    marginTop: '8%',
    justifyContent: 'center',
    alignItems: 'center'

  },
  updatetxt: {
    color: 'white',
    fontSize: 18,
    fontWeight: '500',
    textAlign: 'center'
  },
  inputtxt: {
    color: 'black',
    marginHorizontal: '5%',
    fontSize: 16
  },
  profilegradient: {
    paddingLeft: 15,
    height: 180,
    width: '100%',
    paddingRight: 15,
    borderRadius: 5
  }

})