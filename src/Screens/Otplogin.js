import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native'
import React from 'react'
import Logo from '../assets/images/logo.svg';
import { useNavigation } from '@react-navigation/native';
import Otpcode from '../components/Otp';

const Otplogin = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <TouchableOpacity style={{ width: '100%', alignItems: 'flex-end', }} onPress={() => navigation.navigate('home')}>
        <Text style={{ color: 'black', fontSize: 20, margin: 15 }}>Skip</Text>
      </TouchableOpacity>
      <Logo width={250} height={250} style={{ marginVertical: "6%" }} />
      <TextInput style={styles.Otp}
        placeholder='Mobile Number'
        keyboardType="phone-pad"
        maxLength={10}
        placeholderTextColor='black'
        fontSize={17}
        fontWeight={'500'}
      />
      <Otpcode />
      <Text style={{ color: 'grey', fontSize: 16, fontWeight: '500', marginVertical: 20 }}>Code was send your mobile phone</Text>
      <TouchableOpacity>
        <Text style={{ color: 'blue', fontSize: 18, fontWeight: 'bold', marginVertical: 20 }}>Resend</Text>
      </TouchableOpacity>
    </View>
  )
}

export default Otplogin;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  Otp: {
    height: 60,
    width: '82%',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    color: 'black',
    fontSize: 17,
    fontWeight: '500',
  },
  Otptext: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  Login: {
    height: 60,
    width: '84%',
    borderRadius: 20,
    borderWidth: 1.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'black',
    marginVertical: 20,
  },
  logintext: {
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
  },
  skip: {
    height: 60,
    width: '60%',
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
    backgroundColor: 'black',
    marginTop: 40,
  },
  skiptext: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
})