import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import Logo from '../assets/images/logo.svg';
import { useNavigation } from '@react-navigation/native';

const Login = () => {
    const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <Logo width={250} height={250} style={{marginVertical:"10%"}} />
            <TouchableOpacity style={styles.Otp} onPress={()=>navigation.navigate('Otp')}>
                <Text style={styles.Otptext}>OTP Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.Login}>
                <Text style={styles.logintext}>New User</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.skip} onPress={()=> navigation.navigate('home')}>
                <Text style={styles.skiptext}>Skip</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center'
    },
    Otp: {
        height: 60,
        width: '84%',
        backgroundColor: 'black',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:"15%"

    },
    Otptext: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
    },
    Login: {
        height: 60,
        width: '84%',
        borderRadius: 20,
        borderWidth: 1.5,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'black',
        marginVertical: 20
    },
    logintext: {
        fontSize: 20,
        color: 'black',
        fontWeight: 'bold',
    },
    skip: {
        height: 60,
        width: '60%',
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 20,
        backgroundColor: 'black',
        marginTop: 40
    },
    skiptext: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
    }
})