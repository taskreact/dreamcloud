/* eslint-disable react-native/no-inline-styles */
import { FlatList, SafeAreaView, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import Progress from 'react-native-progress/Bar';
import Featur from '../components/Example';

const Feature = () => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flexDirection: 'row', width: '92%', justifyContent: 'space-between',marginVertical:5 ,marginLeft:10}}>
        <Text style={styles.txt}>Featured</Text>
        <TouchableOpacity>
          <Text style={{ color: 'blue', marginTop: 7 }}>View all</Text>
        </TouchableOpacity>
      </View>
      <View style={{
        marginLeft: 20, shadowOffset:
        {
          width: 10,
          height: 10,
        },
        shadowColor: 'black',
        shadowOpacity: 1.0,
      }}>
        <FlatList
          horizontal
          data={Featur}
          renderItem={({ item }) =>
            <TouchableOpacity style={styles.product}>
              <Image source={{ uri: item?.image }} style={{ height: '55%', width: '100%', resizeMode: 'stretch', borderTopLeftRadius: 12, borderTopRightRadius: 12 }} />
              <Text style={{ color: 'black', fontSize: 15, fontWeight: '500' }}>{item?.add}</Text>
              <Text style={{ color: 'black', }}>{item?.name}</Text>
              <Progress progress={item?.progress} width={100} height={5} color="orange" borderWidth={1} borderRadius={12} marginTop={12} />
            </TouchableOpacity>
          }
        />
      </View>
    </SafeAreaView>
  );
}

export default Feature;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 210,
    marginLeft:5,
    // alignItems: 'center',
    // justifyContent: 'center',
    marginVertical: 10,
  },
  product: {
    height: '100%',
    width: 140,
    backgroundColor: 'white',
    marginHorizontal: 7,
    borderRadius: 10,
    alignItems: 'center',
    marginTop:5
  },
  txt: {
    color: 'black',
    fontSize: 20,
    fontWeight: '500'
  }
})