import { FlatList, Image, StyleSheet, View, Text, ImageBackground, Dimensions } from 'react-native'
import React, { useState } from 'react'
import Featur from './Example';

const { height, width } = Dimensions.get('window')


const SliderItem = () => {
  const [active, setActive] = useState(0);



  return (
    <View style={styles.container}>
      <FlatList
        horizontal
        pagingEnabled
        data={Featur}
        onScroll={e => {
          const x = e.nativeEvent.contentOffset.x;
          setActive((x / width).toFixed(0));
        }}
        renderItem={({ item }) =>
          <View style={styles.product}>
            <ImageBackground borderRadius={12} resizeMode='stretch'
              source={{ uri: item?.Banner }} style={{ height: '100%', width: 365, alignItems: 'flex-end', }} >
            </ImageBackground>
          </View>
        }
      />
      <View style={{ height: 18, width: 70, position: 'absolute', bottom: 3, borderRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'row' }}>
        {
          Featur.map((item) => {
            return (
              <Text style={{ alignSelf: 'flex-start', fontSize: 17, color: active == item?.id ? 'white' : '#9C978E' }}>●</Text>
            )
          })
        }
      </View>
    </View>
  );
};

export default SliderItem;

const styles = StyleSheet.create({
  container: {
    height:170,
    width: '100%',
    paddingHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft:0
  },
  product: {
    height: '100%',
    marginRight: 10
  }
})