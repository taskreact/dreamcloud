import { StyleSheet, Text, TextInput, View } from 'react-native';
import React from 'react';

const inputs = Array(4).fill(0)

const Otpcode = () => {
    return (
        <View style={styles.Otpview}>
            {
                inputs.map((index, i) => {
                    return (
                        <TextInput style={styles.otp} autoFocus keyboardType="phone-pad" maxLength={1} />
                    );
                })
            }
            {/* <TextInput style={styles.otp} autoFocus keyboardType="phone-pad" maxLength={1} />
            <TextInput style={styles.otp} autoFocus keyboardType="phone-pad" maxLength={1} />
            <TextInput style={styles.otp} autoFocus keyboardType="phone-pad" maxLength={1} /> */}
        </View>
    )
}

export default Otpcode

const styles = StyleSheet.create({
    Otpview: {
        height: 60,
        width: '70%',
        marginTop: '10%',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    otp: {
        height: '100%',
        width: 60,
        backgroundColor: 'white',
        borderRadius: 10,
        color: 'black',
        textAlign: 'center',
        fontSize: 17,
        fontWeight: '500',
    },
})