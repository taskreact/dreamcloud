import { StyleSheet, Text, View,Image } from 'react-native'
import React from 'react'
import Homescreen from '../Screens/Homescreen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '../assets/images/home.svg'
import Cart from '../assets/images/cart.svg'
import Person from '../assets/images/person.svg'
import Win from '../assets/images/win.svg'
import Profile from '../Screens/Profile';
import Game from '../Screens/Game';
import Cartpage from '../Screens/Cartpage';

const Bottom = () => {
const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator screenOptions={{
      tabBarStyle: {
        backgroundColor: 'white',
        width:'100%',
        height:60,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
      },
      
    }}>
      <Tab.Screen name="Home" component={Homescreen}  options={{
          headerShown:false,
          tabBarIcon: () => {
            return (
              <Home width={30} height={30}/>
            );
          },
        }} />
      <Tab.Screen name="Game" component={Game}  options={{headerShown:false,
      tabBarIcon: () => {
            return (
              <Win width={30} height={30}/>
            );
          },}} />
      <Tab.Screen name="Cart" component={Cartpage}  options={{headerShown:false,
      tabBarIcon: () => {
            return (
              <Cart width={30} height={30} />
            );
          },}} />
      <Tab.Screen name="Profile" component={Profile}  options={{headerShown:false,
      tabBarIcon: () => {
            return (
              <Person width={30} height={30}/>
            );
          },}} />
    </Tab.Navigator>
  );
};

export default Bottom

const styles = StyleSheet.create({})