import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import Notification from '../assets/images/notification.svg'
import Search from '../assets/images/search.svg'

const Header = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.headertxt}>DreamCloud</Text>
            <View style={{ margin: 8, flexDirection: 'row', width: '18%', justifyContent: 'space-between' }}>
                <TouchableOpacity>
                    <Search width={30} height={30} />
                </TouchableOpacity>
                <Notification width={30} height={30} />
            </View>
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    container: {
        height: 56,
        width: '96%',
        flexDirection: 'row',
        justifyContent: 'space-between'

    },
    headertxt: {
        color: 'black',
        margin: 10,
        fontSize: 20,
        fontWeight: '900',
        marginHorizontal: 20
    }
})