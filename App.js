import { View, Text, SafeAreaView,Image } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from './src/Screens/Login';
import Bottom from './src/components/Bottom';
import Otplogin from './src/Screens/Otplogin';



const Stack = createNativeStackNavigator();

export default function App() {
 
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="login" component={Login}  />
        <Stack.Screen name="Otp" component={Otplogin}  />
        <Stack.Screen name="home" component={Bottom} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
